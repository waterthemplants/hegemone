use serde::{Serialize, Deserialize};

#[derive(Copy, Clone, Debug, Serialize, Deserialize)]
pub struct LightConfig {
    pub gpio_pin: u32,
    pub seconds_period: u64,
    pub seconds_on: u64,
    pub seconds_offset: u64,
    pub enabled: bool,
}

#[derive(Copy, Clone, Debug, Serialize, Deserialize)]
pub struct PumpConfig {
    pub gpio_pin: u32,
    pub seconds_period: u64,
    pub seconds_on: u64,
    pub seconds_offset: u64,
    pub enabled: bool,
}

#[derive(Copy, Clone, Default, Debug, Serialize, Deserialize)]
pub struct Config {
    pub pump: PumpConfig,
    pub light: LightConfig,
}

impl Default for LightConfig {
    fn default() -> Self {
        LightConfig {
            gpio_pin: 26,
            seconds_period: 86400,
            seconds_on: 57600,
            seconds_offset: 0,
            enabled: true,
        }
    }
}

impl Default for PumpConfig {
    fn default() -> Self {
        PumpConfig {
            gpio_pin: 20,
            seconds_period: 3600,
            seconds_on: 40,
            seconds_offset: 3600,
            enabled: true,
        }
    }
}
