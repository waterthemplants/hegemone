use super::config::Config;
use std::time::Instant;

#[derive(Copy, Clone)]
pub struct Schedule {
    pub t0: Instant,
    config: Config
}

impl Schedule {
    pub fn new(now: Instant, config: Config) -> Self {
        Schedule {
            t0: now,
            config
        }
    }

    fn target_square_wave_state(
        self,
        t_s: Instant,
        t0_s: Instant,
        seconds_period: u64,
        seconds_on: u64,
        seconds_offset: u64) -> bool {
        // Map the current time onto the output state of a square wave with
        // a specified period and duty cycle and phase shift in seconds.
        // Here we take the convention that a phase shift of zero implies
        // that the wave is in the 'true' state at t=0.
        let (lower_edge, upper_edge) = (seconds_offset, seconds_offset + seconds_on);

        let time_mod = t_s.duration_since(t0_s).as_secs() % (seconds_period as u64);
        (time_mod >= lower_edge) && (time_mod <= upper_edge)
    }


    pub fn target_pump_state(self, current_time: Instant) -> bool {
        if self.target_light_state(current_time) == false {
            return false
        }

        self.target_square_wave_state(
            current_time,
            self.t0,
            self.config.pump.seconds_period,
            self.config.pump.seconds_on,
            self.config.pump.seconds_offset,
        )
    }

    pub fn target_light_state(self, current_time: Instant) -> bool {
        self.target_square_wave_state(
            current_time,
            self.t0,
            self.config.light.seconds_period,
            self.config.light.seconds_on,
            self.config.light.seconds_offset,
        )
    }
}

#[cfg(test)]
mod tests {
    use super::{Config, Schedule};
    use std::time::{Instant, Duration};

    #[test]
    fn light_on_at_t0() {
        let cfg = Config::default();
        let now = Instant::now();
        let sched = Schedule::new(now, cfg);

        assert_eq!(sched.target_light_state(now), true);
    }

    #[test]
    fn pump_on_at_t0_with_zero_phase() {
        let mut cfg = Config::default();
        cfg.pump.seconds_offset = 0;
        let now = Instant::now();
        let sched = Schedule::new(now, cfg);

        assert_eq!(sched.target_pump_state(now), true);
    }

    #[test]
    fn pump_off_at_t0_with_nonzero_phase() {
        let mut cfg = Config::default();
        cfg.pump.seconds_offset = 10;
        let now = Instant::now();
        let sched = Schedule::new(now, cfg);

        assert_eq!(sched.target_pump_state(now), false);
    }

    #[test]
    fn pump_on_at_t0_plus_period() {
        let mut cfg = Config::default();
        cfg.pump.seconds_offset = 0;
        let now = Instant::now();
        let period_1_start = now + Duration::from_secs(cfg.pump.seconds_period);
        let sched = Schedule::new(now, cfg);

        assert_eq!(sched.target_pump_state(period_1_start), true);
    }
}
