mod config;
mod schedule;
use std::time::Instant;
use config::Config;
use schedule::Schedule;
use std::{time, thread};
use gpio_cdev::{Chip, LineRequestFlags};

fn convert_state_to_value(target_state: bool, device: &str) -> u8 {
    // because the relays are default ON (when the program is not running),
    // we are using the IoT relay to invert that signal (so that the light is OFF by default).
    // in order to have the program make sense, we invert the target for the IoT relay.
    // which means we invert the light. but not the pump (for some reason, I don't understand).
    // true=>0, false=>1
    if device == "light" {
        return (target_state == false) as u8;
    } else {
        return target_state as u8;
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    // Read configuration data
    // first check ~/.config.yml, then look in current working dir + config.yml
    let mut config_path = std::path::Path::new(&std::env::var("HOME").unwrap()).join(".config.yml");
    if !config_path.is_file() {
        config_path = std::path::Path::new("config.yml").to_path_buf();
    }
    if !config_path.is_file() {
        panic!("no config file found - please add config file in ~/.config.yml or in <current working dir>/config.yml")
    }
    let config_contents = std::fs::File::open(config_path)?;
    let config: Config = serde_yaml::from_reader(config_contents)?;

    // Create handles to pins
    let mut gpio_chip = Chip::new("/dev/gpiochip0")?;
    let light = gpio_chip
        .get_line(config.light.gpio_pin)?
        .request(LineRequestFlags::OUTPUT, 1, "light")?;
    let pump = gpio_chip
        .get_line(config.pump.gpio_pin)?
        .request(LineRequestFlags::OUTPUT, 1, "pump")?;

    // loop forever, flipping the pump and light states according the code in sched
    let mut current_light_state = false; // light starts in default OFF
    let mut current_pump_state = true; // pump starts in default ON
    let mut cycle_number = 0;
    let mut now = Instant::now();
    let sched = Schedule::new(now, config);
    loop {
        now = Instant::now();
        let seconds_elapsed = now.duration_since(sched.t0).as_secs();
        let target_pump_state = sched.target_pump_state(now);
        let target_light_state = sched.target_light_state(now);

        if target_pump_state != current_pump_state {
            if !(target_pump_state && !config.pump.enabled) {
                pump.set_value(convert_state_to_value(target_pump_state, "pump"))?;
                current_pump_state = target_pump_state;
                println!("{}: set pump state: {}", seconds_elapsed, target_pump_state);
            }
        }
        if target_light_state != current_light_state {
            if !(target_light_state && !config.light.enabled) {
                light.set_value(convert_state_to_value(target_light_state, "light"))?;
                current_light_state = target_light_state;
                println!("{}: set light state: {}", seconds_elapsed, target_light_state);
            }
        }
        thread::sleep(time::Duration::from_secs(1));
        cycle_number = cycle_number+1;
    }
}
