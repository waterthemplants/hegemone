build:
	cross build --target arm-unknown-linux-gnueabi

upload: build
	scp target/arm-unknown-linux-gnueabi/debug/hegemone pi@raspberrypi.local:/usr/bin/hegemone

upload_config:
	scp config.yml pi@raspberrypi.local:~/.config.yml

upload_config_test:
	scp config-test.yml pi@raspberrypi.local:~/.config.yml

upload_config_seedling:
	scp config-seedling.yml pi@raspberrypi.local:~/.config.yml

start:
	ssh pi@raspberrypi.local "nohup hegemone &> /var/log/hegemone.log &"

stop:
	ssh pi@raspberrypi.local "killall hegemone"

# can see logs on machine with: tail -f  /var/log/hegemone.log
build_os_image_builder:
	docker build -t os_img_builder:latest -f build/Dockerfile build/

build_os_image:
	docker run\
		--privileged\
		--cap-add SYS_ADMIN\
		-e WIFI_SSID\
		-e WIFI_PASSWORD\
		-v $(shell pwd):/work:Z\
		-t os_img_builder:latest
