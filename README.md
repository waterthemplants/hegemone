# Installation

1. [Install rust](https://rustup.rs/)
2. go to this directory
3. `cargo test` should result in a bunch of green lights

If you want to build for the pi, you'll need a few more things
1. Install `cross`: `cargo install cross`
2. install [Docker](https://www.docker.com/products/docker-desktop) and open it

# How to build
`make build`

# How to send code to pi
`make upload`
