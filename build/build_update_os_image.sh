#!/usr/bin/env bash
BASE='/2020-02-13-raspbian-buster-lite.img'
WORK='/raspbian_latest.img'

echo $WIFI_SSID
echo $WIFI_PASSWORD

cp $BASE $WORK

echo 'writing root...'
first_root_sector=$(fdisk -lu $WORK | grep 'Linux' | awk '{print $2 * 512}')
mkdir -p /mnt/iso
mount -t auto -o loop,offset=$first_root_sector $WORK /mnt/iso
cp work/target/arm-unknown-linux-gnueabi/debug/hegemone /mnt/iso/usr/bin/
echo "/usr/bin/hegemone" >> /mnt/iso/etc/rc.local
umount /mnt/iso

echo 'writing boot...'
first_boot_sector=$(fdisk -lu $WORK | grep 'W95' | awk '{print $2 * 512}')
mkdir -p /mnt/boot
mount -t auto -o loop,offset=$first_boot_sector $WORK /mnt/boot
touch /mnt/boot/ssh
touch /mnt/boot/wpa_supplicant.conf
cat << EOF >> /mnt/boot/wpa_supplicant.conf
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
country=US

network={
   ssid="$WIFI_SSID"
   proto=RSN
   key_mgmt=WPA-PSK
   pairwise=CCMP TKIP
   group=CCMP TKIP
   psk="$WIFI_PASSWORD"
}
EOF
umount /mnt/boot

mv $WORK /work$BASE
